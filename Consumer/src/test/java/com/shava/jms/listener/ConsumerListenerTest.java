package com.shava.jms.listener;

import javax.jms.JMSException;
import javax.jms.TextMessage;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ConsumerListenerTest {
	
	private TextMessage textMessage;
	
	private ApplicationContext context;
	
	private ConsumerListener listener;
	
	private String json ="{vendorName:\"Microsofttest3\",firstName:\"BobTest3\",lastName:\"SmithTest3\",address:\"123 Main test3\",city:\"TulsaTest3\",state:\"OKTest3\",zip:\"71345Test3\",email:\"Bob@microsoft.test3\",phoneNumber:\"test-123-test3\"}";

	@Before
	public void setUp() throws Exception {
		context = new ClassPathXmlApplicationContext("spring/application-config.xml");
		listener = (ConsumerListener)context.getBean("consumerListener");
		textMessage= EasyMock.createMock(TextMessage.class);
	}

	@After
	public void tearDown() throws Exception {
		((ConfigurableApplicationContext)context).close();
	}

	@Test
	public void testOnMessage() throws JMSException {
		EasyMock.expect(textMessage.getText()).andReturn(json);
		EasyMock.replay(textMessage);
		listener.onMessage(textMessage);
		EasyMock.verify(textMessage);
	}

}
